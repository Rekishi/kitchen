﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using TheKitchen.Services;

namespace TheKitchen.Droid
{
    [Activity(Label = "OAuth2Activity")]
    [IntentFilter(
    actions: new[] { Intent.ActionView },
    Categories = new[] { Intent.CategoryBrowsable, Intent.CategoryDefault },
    DataScheme = GoogleOAuthManager.REDIRECT_SCHEME,
    DataPath = GoogleOAuthManager.REDIRECT_PATH
)]
    public class OAuth2Activity : AppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            Uri uri = new Uri(Intent.Data.ToString());
            GoogleOAuthManager.Authenticator.OnPageLoading(uri);

            Finish();
        }
    }
}