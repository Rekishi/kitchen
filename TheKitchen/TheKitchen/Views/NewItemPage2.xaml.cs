﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using TheKitchen.Models;
using System.Diagnostics;
using System.Linq;

namespace TheKitchen.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NewItemPage2 : ContentPage
    {
        public ConsumableItem Item { get; set; }
        public List<String> Errors { get; set; }

        public NewItemPage2()
        {
            InitializeComponent();
            Item = ConsumableItem.GetNewItem(true);
            BindingContext = this;
        }

        async void Save_Clicked(object sender, EventArgs e)
        {
            try
            {

                var errCheck = Item.Validate();

                if (errCheck.HasErrors())
                {
                    await DisplayAlert("Alert", errCheck.GetErrors().First(), "OK");
                }
                else
                {
                    MessagingCenter.Send(this, "AddItem", Item);
                    await Navigation.PopModalAsync();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }
    }
}