﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheKitchen.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TheKitchen.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        LoginViewModel viewModel;

        public LoginPage()
        {
            InitializeComponent();
            BindingContext = this.viewModel = new LoginViewModel();

            viewModel.PropertyChanged += ViewModel_PropertyChanged;

            //NavigateToMain();

        }

        async void Login_Clicked(object sender, EventArgs e)
        {
            await viewModel.Login();
        }


        void NavigateToMain()
        {
             Application.Current.MainPage = new MainPage();
        }

        void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "LoggedIn")
            {
                if (viewModel.LoggedIn == true)
                {
                    NavigateToMain();

                    //Task.Run(async () =>
                    //{
                    //    SettingsViewModel set = new SettingsViewModel();
                    //    await set.Sync();
                    //});
                }
            }
        }

    }
}