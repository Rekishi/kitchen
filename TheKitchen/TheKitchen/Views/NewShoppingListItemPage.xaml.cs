﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using TheKitchen.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TheKitchen.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class NewShoppingListItemPage : ContentPage
	{
        public ConsumableItem Item { get; set; }
        public List<String> Errors { get; set; }

        public NewShoppingListItemPage()
        {
            InitializeComponent();
            Item = ConsumableItem.GetNewItem(false);
            BindingContext = this;
        }

        async void Save_Clicked(object sender, EventArgs e)
        {
            try
            {

                var errCheck = Item.Validate();

                if (errCheck.HasErrors())
                {
                    await DisplayAlert("Alert", errCheck.GetErrors().First(), "OK");
                }
                else
                {
                    MessagingCenter.Send(this, "AddItem", Item);
                    await Navigation.PopModalAsync();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }
    }
}