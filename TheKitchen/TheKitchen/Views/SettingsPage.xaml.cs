﻿using System;
using System.Threading.Tasks;
using TheKitchen.Models;
using TheKitchen.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TheKitchen.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SettingsPage : ContentPage
    {
        SettingsViewModel viewModel;

        public Command RestoreCommand { get; set; }

        public SettingsPage()
        {
            InitializeComponent();

            BindingContext = this.viewModel = new SettingsViewModel();
            RestoreCommand = new Command<BackupItem>(async (item) => await RestoreDatabaseCommand(item));

        }

        async void Backup_Clicked(object sender, EventArgs e)
        {
            await viewModel.Backup();
        }

        async void Sync_Clicked(object sender, EventArgs e)
        {
            if (!await viewModel.Sync())
            {
                await DisplayAlert("Alert", "There was a problem authorizing you with the server.  Please try to login again.", "OK");
                //await new LoginViewModel().PromptLogin(true);
                await Navigation.PushAsync(new LoginPage());
            }
            else
            {
                await DisplayAlert("Success", "Your information has now been synchronized", "OK");
            }
        }

        async Task RestoreDatabaseCommand(BackupItem item)
        {
            await viewModel.RestoreBackup(item);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            //if (viewModel.Backups.Count == 0)
            viewModel.LoadItemsCommand.Execute(null);
        }
    }
}