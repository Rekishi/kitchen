﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using TheKitchen.Models;
using TheKitchen.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TheKitchen.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ShoppingListPage : ContentPage
    {
        ShoppingListViewModel viewModel;

        public ShoppingListPage()
        {
            InitializeComponent();
            BindingContext = viewModel = new ShoppingListViewModel();
        }

        void Selected(object sender, SelectedItemChangedEventArgs e)
        {
            ((ListView)sender).SelectedItem = null;
        }

        async void AddItem_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new NavigationPage(new NewShoppingListItemPage()));
        }

        async void Done_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new NavigationPage(new CommitShoppingPage()));
        }
        
        protected override void OnAppearing()
        {
            base.OnAppearing();
            viewModel.LoadItemsCommand.Execute(null);
        }

        public async void Delete_it(object sender, EventArgs e)
        {
            var mi = ((MenuItem)sender);

            ConsumableItem item = mi.CommandParameter as ConsumableItem;

            await viewModel.DeleteItem(item);
            viewModel.LoadItemsCommand.Execute(null);
        }
    }
}