﻿using System;
using TheKitchen.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TheKitchen.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ConsumeItemPage : ContentPage
    {
        ConsumeItemViewModel viewModel;

        public ConsumeItemPage(ConsumeItemViewModel viewModel)
        {
            InitializeComponent();

            BindingContext = this.viewModel = viewModel;

        }

        async void Save_Clicked(object sender, EventArgs e)
        {
            MessagingCenter.Send(this, "ConsumeItem", viewModel.Item);
            await Navigation.PopModalAsync();
        }

        private void Editor_Focused(object sender, FocusEventArgs e)
        {
            (sender as Xamarin.Forms.Editor).Text = "";
        }
    }
}