﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheKitchen.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TheKitchen.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CommitShoppingPage : ContentPage
    {
        CommitShoppingViewModel viewModel;

        public CommitShoppingPage()
        {
            InitializeComponent();
            BindingContext = viewModel = new CommitShoppingViewModel();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            viewModel.LoadItemsCommand.Execute(null);
        }

        async void gotIt_Clicked(object sender, EventArgs e)
        {
            await viewModel.CommitItems();
            await Navigation.PopModalAsync();
        }


        public void check_it(object sender, EventArgs e)
        {
            //await viewModel.CommitItems();
            //await Navigation.PopModalAsync();
        }



    }
}