﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using TheKitchen.Models;
using TheKitchen.ViewModels;
using System.Linq;
using System.Diagnostics;

namespace TheKitchen.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ItemDetailPage : ContentPage
	{
        ItemDetailViewModel viewModel;

        public ItemDetailPage(ItemDetailViewModel viewModel)
        {
            InitializeComponent();
            BindingContext = this.viewModel = viewModel;
        }
        
        async void Save_Clicked(object sender, EventArgs e)
        {
            try
            {

                var errCheck = viewModel.Item.Validate();

                if (errCheck.HasErrors())
                {
                    await DisplayAlert("Alert", errCheck.GetErrors().First(), "OK");
                }
                else
                {
                    await viewModel.SaveItem();
                    await Navigation.PopAsync();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }
    }
}