﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using TheKitchen.Models;
using TheKitchen.Views;
using Xamarin.Forms;
using System.ComponentModel;
using System.Linq;

namespace TheKitchen.ViewModels
{
    public class CommitShoppingViewModel : BaseViewModel
    {
        public ObservableCollection<ConsumableItem> Items { get; set; }
        public Command LoadItemsCommand { get; set; }

        public CommitShoppingViewModel()
        {
            Title = "Done Shopping";
            Items = new ObservableCollection<ConsumableItem>();
            LoadItemsCommand = new Command(async () => await ExecuteLoadItemsCommand());
        }

        public async Task CommitItems()
        {
            foreach (var item in Items)
            {
                item.OnHand = true;
                item.GotItShoppingDate = DateTime.Now;
                await DataStore.UpdateItemAsync(item);
            }
        }

        public async Task ExecuteLoadItemsCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                Items.Clear();

                var items = await DataStore.GetItemsAsync(false);

                var gotItems = items.Where(x => x.GotItShopping == true && x.OnHand == false).OrderBy(x => x.Name);

                foreach (var item in gotItems)
                {
                    //item.PropertyChanged += ItemToggled;
                    Items.Add(item);
                }

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }


        
    }
}
