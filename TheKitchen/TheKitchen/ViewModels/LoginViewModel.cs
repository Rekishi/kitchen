﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TheKitchen.Models;
using TheKitchen.Services;
using TheKitchen.Services.DataStores;
using Xamarin.Auth;
using Xamarin.Forms;

namespace TheKitchen.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        bool stayLoggedIn = false;
        public bool StayLoggedIn
        {
            get { return stayLoggedIn; }
            set { SetProperty(ref stayLoggedIn, value); }
        }

        bool loggedIn = false;
        public bool LoggedIn
        {
            get { return loggedIn; }
            set { SetProperty(ref loggedIn, value); }
        }

        string loginStatus = "Please Log In";
        public string LoginStatus
        {
            get { return loginStatus; }
            set { SetProperty(ref loginStatus, value); }
        }

        public ICommand LoginCommand { get; private set; }


        public LoginViewModel()
        {
            Title = "Login";

            LoginCommand = new Command(async () => await Login());

            GoogleOAuthManager.Authenticator.Completed += OnAuthenticatorCompleted;

            
        }



        public async Task PromptLogin(bool forceLogin = false)
        {

            var authCheck = await new TokenDataStore().GetItemAsync(Guid.Empty);
            if (!forceLogin)
            {
                if (authCheck != null)
                {
                    if (authCheck.ExpireDateTime > DateTime.Now) // if expired, force to get a new one
                    {
                        StaticConfigs.Token = authCheck.id_token;
                        LoggedIn = true;
                        return;
                    }
                }
            }

#if __ANDROID__
            Android.Content.Intent intent = GoogleOAuthManager.Authenticator.GetUI((Android.App.Activity)Forms.Context);

            Forms.Context.StartActivity(intent);
#endif
        }


        private async void OnAuthenticatorCompleted(object sender, AuthenticatorCompletedEventArgs e)
        {
            // Here you can use e.Account.Properties["id_token"] to get a JWS which contains the username

            if (e.IsAuthenticated)
            {
                // todo - Get and save token
                // also check to see if the user select to stay logged in

                // redirect to main page
                // expires_in is in seconds

                LoggedIn = true;

                var authInfo = new LoginToken()
                {
                    access_token = e.Account.Properties["access_token"],
                    expires_in = e.Account.Properties["expires_in"],
                    id_token = e.Account.Properties["id_token"],
                    refresh_token = e.Account.Properties["refresh_token"],
                    token_type = e.Account.Properties["token_type"],
                };

                StaticConfigs.Token = authInfo.id_token;
                //await App.Current.MainPage.DisplayAlert("Set new token", StaticConfigs.Token, "OK");

                try
                {
                    await new TokenDataStore().AddItemAsync(authInfo);
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
            else
            {
                // Show error message
                LoginStatus = "Login failed...";
            }

        }
        
        public async Task Login()
        {
            LoginStatus = "Logging In";
            await PromptLogin();
            //LoggedIn = true;
        }



    }
}
