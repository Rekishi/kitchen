﻿using System;
using System.Threading.Tasks;
using TheKitchen.Models;

namespace TheKitchen.ViewModels
{
    public class ItemDetailViewModel : BaseViewModel
    {
        public ConsumableItem Item { get; set; }
        public ItemDetailViewModel(ConsumableItem item = null)
        {
            this.Item = item;
        }

        public async Task SaveItem()
        {
            await DataStore.UpdateItemAsync(Item);
        }
    }
}
