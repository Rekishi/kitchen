﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Forms;
using TheKitchen.Models;
using TheKitchen.Views;
using System.Collections.Generic;
using System.Linq;

namespace TheKitchen.ViewModels
{
    public class ItemsViewModel : BaseViewModel
    {
        public ObservableCollection<ConsumableItem> Items { get; set; }
        private IEnumerable<ConsumableItem> AllItems { get; set; }
        
        public Command LoadItemsCommand { get; set; }

        string searchText = "";
        public string SearchText
        {
            get { return searchText; }
            set { SetProperty(ref searchText, value); }
        }

        public ItemsViewModel()
        {
            Title = "Here's what's in your Pantry";
            Items = new ObservableCollection<ConsumableItem>();
            LoadItemsCommand = new Command(async () => await ExecuteLoadItemsCommand());

            MessagingCenter.Subscribe<NewItemPage2, ConsumableItem>(this, "AddItem", async (obj, item) =>
            {
                var _item = item as ConsumableItem;
                Items.Add(_item);
                await DataStore.AddItemAsync(_item);
            });


            MessagingCenter.Subscribe<ConsumeItemPage, ConsumableItem>(this, "ConsumeItem", async (obj, item) =>
            {
                var _item = item as ConsumableItem;
                await DataStore.ConsumeItemAsync(_item);
                await ExecuteLoadItemsCommand();
            });

            this.PropertyChanged += ItemsViewModel_PropertyChanged;
        }

        private void ItemsViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "SearchText")
            {
                SetItems();
            }
        }

        void SetItems()
        {
            Items.Clear();
            var filtered = AllItems.Where(x => x.Name.ToLower().Contains(SearchText.ToLower()));
            foreach (var item in filtered)
            {
                Items.Add(item);
            }
            
        }



        async Task ExecuteLoadItemsCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                var items = await DataStore.GetItemsAsync(true);
                AllItems = items;
                SetItems();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}