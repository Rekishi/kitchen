﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using TheKitchen.Models;
using TheKitchen.Views;
using Xamarin.Forms;
using System.ComponentModel;
using System.Linq;

namespace TheKitchen.ViewModels
{
    public class ShoppingListViewModel : BaseViewModel
    {
        public ObservableCollection<ConsumableItem> Items { get; set; }
        public Command LoadItemsCommand { get; set; }

        public ShoppingListViewModel()
        {
            Title = "Here's your shopping list";
            Items = new ObservableCollection<ConsumableItem>();
            LoadItemsCommand = new Command(async () => await ExecuteLoadItemsCommand());

            MessagingCenter.Subscribe<NewShoppingListItemPage, ConsumableItem>(this, "AddItem", async (obj, item) =>
            {
                item.PropertyChanged += ItemToggled;
                Items.Add(item);
                await DataStore.AddItemAsync(item);
            });

        }

        async void ItemToggled(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(ConsumableItem.GotItShopping))
            {
                var item = sender as ConsumableItem;

                // save item
                await DataStore.UpdateItemAsync(item);

                // move position in list
                if (item.GotItShopping == true)
                {
                    Items.Remove(item);
                    Items.Add(item);
                }
                else
                {
                    Items.Remove(item);
                    Items.Insert(0, item);
                }

            }
        }

        public async Task DeleteItem(ConsumableItem item)
        {
            await DataStore.DeleteItemAsync(item);
        }

        public async Task ExecuteLoadItemsCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                Items.Clear();

                var items = await DataStore.GetItemsAsync(false);

                var gotItems = items.Where(x => x.GotItShopping == true).OrderBy(x => x.Name);
                var unGotItems = items.Where(x => x.GotItShopping == false).OrderBy(x => x.Name);

                foreach (var item in unGotItems)
                {
                    item.PropertyChanged += ItemToggled;
                    Items.Add(item);
                }

                foreach (var item in gotItems)
                {
                    item.PropertyChanged += ItemToggled;
                    Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
