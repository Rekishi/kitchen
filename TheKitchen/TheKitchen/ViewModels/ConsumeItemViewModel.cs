﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using TheKitchen.Models;
using TheKitchen.Services.Misc;
using Xamarin.Forms;

namespace TheKitchen.ViewModels
{
    public class ConsumeItemViewModel : BaseViewModel
    {

        public ConsumableItem Item { get; set; }

        public ObservableCollection<MeasureConversion> MeasureConversions { get; set; }

        decimal useQuantity;
        public decimal UseQuantity
        {
            get { return useQuantity; }
            set { SetProperty(ref useQuantity, value); }
        }

        public ConsumeItemViewModel(ConsumableItem item)
        {
            Item = item;
            this.UseQuantity = Item.Quantity;
            MeasureConversions = new ObservableCollection<MeasureConversion>();
            SetMeasureConversions();
            this.PropertyChanged += ConsumeItemViewModel_PropertyChanged;
        }

        private void ConsumeItemViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "UseQuantity")
            {
                UseQuantity = Math.Round(UseQuantity, 2);
                this.Item.Quantity = UseQuantity;
            }
        }


        private void SetMeasureConversions()
        {
            var resTemp = new List<MeasureConversion>();

            var allMeasures = Enum.GetValues(typeof(UnitMeasure)).Cast<UnitMeasure>();
            var thisMeasureType = Item.UnitOfMeasure.GetAttribute<DisplayAttribute>().MeasureType;
            foreach (var item in allMeasures)
            {
                var measureType = item.GetAttribute<DisplayAttribute>().MeasureType;
                if (thisMeasureType == measureType
                    && Item.UnitOfMeasure != item)
                {
                    var measureConversionItem = new MeasureConversion()
                    {
                        UnitOfMeasure = item,
                        Quantity = UnitConversion.Convert(Item.Quantity, Item.UnitOfMeasure, item)
                    };

                    measureConversionItem.ConvertEvent += MeasureConversionItem_ConvertEvent;

                    resTemp.Add(measureConversionItem);
                }
            }

            MeasureConversions.Clear();
            foreach (var item in resTemp)
            {
                MeasureConversions.Add(item);
            }
            
        }

        private void MeasureConversionItem_ConvertEvent(object sender, EventArgs e)
        {
            var convItem = (MeasureConversion)sender;
            UseQuantity = UnitConversion.Convert(convItem.Quantity, convItem.UnitOfMeasure, Item.UnitOfMeasure);
            SetMeasureConversions();

        }

    }


    public class MeasureConversion : BaseViewModel
    {
        public Command ConvertedCommand { get; set; }

        public MeasureConversion()
        {
            ConvertedCommand = new Command(() => ConvertEvent(this, new EventArgs()));
        }

        public event EventHandler ConvertEvent;

        decimal quantity;
        public decimal Quantity
        {
            get { return quantity; }
            set { SetProperty(ref quantity, value); }
        }

        public UnitMeasure UnitOfMeasure { get; set; }

        //public async void Convert(object sender, EventArgs e)
        //{
        //}

        public string UnitOfMeasureStringShort
        {
            get
            {
                return UnitMeasureDisplay(x => x.Short);
            }
        }
        public string UnitOfMeasureStringLong
        {
            get
            {
                return UnitMeasureDisplay(x => x.Long);
            }
        }
        private string UnitMeasureDisplay(Func<DisplayAttribute, string> func)
        {
            return func.Invoke(UnitOfMeasure.GetAttribute<DisplayAttribute>());
        }
    }
}
