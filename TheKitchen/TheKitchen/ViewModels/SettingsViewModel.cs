﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TheKitchen.Models;
using TheKitchen.Services;
using Xamarin.Forms;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Linq;
using static TheKitchen.KitchenDatabase;
using TheKitchen.Data.Models;

namespace TheKitchen.ViewModels
{
    public class SettingsViewModel : BaseViewModel
    {
        //private string baseUrl = "http://10.0.2.2:53592/";
        string baseUrl = "http://test.pawtawk.com/";

        private static readonly HttpClient client = new HttpClient();

        private string syncItemsUrl = "api/SaveItemsFromApp";
        private string syncLogUrl = "api/SyncAppLog";

        public ObservableCollection<BackupItem> Backups { get; set; }
        public Command LoadItemsCommand { get; set; }

        bool uIEnabled = true;
        public bool UIEnabled
        {
            get { return uIEnabled; }
            set { SetProperty(ref uIEnabled, value); }
        }

        string appVersion;
        public string AppVersion
        {
            get { return appVersion; }
            set { SetProperty(ref appVersion, value); }
        }

        public SettingsViewModel()
        {

            Backups = new ObservableCollection<BackupItem>();
            LoadItemsCommand = new Command(async () => await ExecuteLoadItemsCommand());

            AppVersion = GetAppversion();

            syncItemsUrl = baseUrl + syncItemsUrl;
            syncLogUrl = baseUrl + syncLogUrl;
        }

        string GetAppversion()
        {
            return "1.1";
        }

        //private string ToJson(IEnumerable<ConsumableItem> dataItems)
        //{
        //    return Newtonsoft.Json.JsonConvert.SerializeObject(dataItems);
        //}

        public async Task<bool> Sync()
        {

            Title = "Syncing...";
            UIEnabled = false;

            bool res = false;
            var token = StaticConfigs.Token;
            //await App.Current.MainPage.DisplayAlert("Using token", token, "OK");

            res = await Task.Run(() => SyncProcess(token));

            //var res = await ();

            Title = "Settings";
            UIEnabled = true;

            return res;
        }


        private async Task<bool> SyncProcess(string token)
        {
            try
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                // send logs
                try
                {
                    var logResp = await PostJson(syncLogUrl, await GetSyncLog());
                    if (logResp.IsSuccessStatusCode && SyncLogsSending != null && SyncLogsSending.Count() > 0)
                    {
                        await DataStore.SetLogEntriesSynced(SyncLogsSending);
                        SyncLogsSending = new List<ActionLogItem>();
                    }
                }
                catch (Exception e) {

                }


                // sync consumables
                var resp = await PostJson(syncItemsUrl, await GetSyncItems());

                if (resp.StatusCode == HttpStatusCode.Unauthorized)
                {
                    return false;
                }

                var cmt = await resp.Content.ReadAsStringAsync();
                await SaveSendBack(cmt);

                await DataStore.StoreMiscInfo(MiscInfoKey.LastSync, DateTime.Now.ToUniversalTime().ToString());


                UIEnabled = true;

                return true;
            }
            catch (Exception e)
            {
                UIEnabled = true;
                return false;
            }
        }

        private async Task<HttpResponseMessage> PostJson(string url, string serializedData)
        {
            var logContent = new StringContent(serializedData, Encoding.UTF8, "application/json");
            return await client.PostAsync(url, logContent);
        }


        private async Task<string> GetSyncItems()
        {

            var lastSyncTime = await GetLastSync();

            var syncItems = new List<ConsumableItem>();
            syncItems.AddRange(await DataStore.GetItemsAsync(false, true));
            syncItems.AddRange(await DataStore.GetItemsAsync(true, true));

            syncItems = syncItems.Where(x => (x.LastModDate ?? DateTime.Now.AddDays(-100)) >= lastSyncTime).ToList();

            return Newtonsoft.Json.JsonConvert.SerializeObject(new { data = syncItems, lastSync = lastSyncTime });

        }

        private IEnumerable<ActionLogItem> SyncLogsSending = new List<ActionLogItem>();

        private async Task<string> GetSyncLog()
        {
            SyncLogsSending = await DataStore.GetLogEntries(false);
            return Newtonsoft.Json.JsonConvert.SerializeObject(SyncLogsSending);
        }

        private async Task<DateTime> GetLastSync()
        {
            //return DateTime.Now.AddDays(-365);
            DateTime ret;
            var lastSync = await DataStore.GetMiscInfo(MiscInfoKey.LastSync);
            if (lastSync == null)
            {
                ret = DateTime.Now.AddDays(-365);
                await DataStore.StoreMiscInfo(MiscInfoKey.LastSync, ret.ToString());
            }
            else
            {
                ret = DateTime.Parse(lastSync.Value);
            }
            return ret;
        }

        private async Task SaveSendBack(string result)
        {
            var items = JsonConvert.DeserializeObject<IEnumerable<ConsumableItem>>(result);

            foreach (var item in items)
            {
                await DataStore.UpdateItemAsync(item, false);
            }
        }



        public async Task Backup()
        {
            var dbHandler = new DatabaseBackupRestoreProxy();

            // future - do this to server
            await dbHandler.Backup();

        }




        public void LoadBackups()
        {
            Backups.Clear();
            var bas = GetLocalBackups().Result;
            foreach (var item in bas)
            {
                Backups.Add(item);
            }
        }


        public async Task RestoreBackup(BackupItem item)
        {
            var dbHandler = new DatabaseBackupRestoreProxy();
            await dbHandler.Restore(item);
        }

        private async Task<List<BackupItem>> GetLocalBackups()
        {
            var dbHandler = new DatabaseBackupRestoreProxy();
            return await dbHandler.GetLocalBackups();
        }

        async Task ExecuteLoadItemsCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                Backups.Clear();
                var items = await GetLocalBackups();
                foreach (var item in items)
                {
                    Backups.Add(item);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }


    }
}
