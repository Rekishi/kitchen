﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Auth;

namespace TheKitchen.Services
{

    public static class GoogleOAuthManager
    {
        private const string CLIENT_ID = "845241831023-04ihdq7ievc8mhcdd0q62470of2aearj.apps.googleusercontent.com";
        private const string CLIENT_SECRET = "bnWiHFqdA0ypiNokZGAbvXPk";
        private const string AUTH_SCOPE = "openid email";               // Or whatever you want
        private const string AUTH_URI = "https://accounts.google.com/o/oauth2/v2/auth";
        //private const string ACCESSTOKEN_URI = "https://accounts.google.com/o/oauth2/token";
        private const string ACCESSTOKEN_URI = "https://accounts.google.com/o/oauth2/token";


        //        public const string REDIRECT_SCHEME = "com.companyname.TheKitchen";    // e.g. com.example.myapp
        public const string REDIRECT_SCHEME = "com.googleusercontent.apps.845241831023-04ihdq7ievc8mhcdd0q62470of2aearj";
        public const string REDIRECT_PATH = "oauth2redirect";                 // You can change this too

        public static readonly OAuth2Authenticator Authenticator;

        static GoogleOAuthManager()
        {
            Authenticator = new OAuth2Authenticator(
                clientId: CLIENT_ID,
                scope: AUTH_SCOPE,
                authorizeUrl: new Uri(AUTH_URI),
                //redirectUrl: new Uri(REDIRECT_SCHEME + ":/" + REDIRECT_PATH),
                //redirectUrl: new Uri("urn:ietf:wg:oauth:2.0:oob"),
                redirectUrl: new Uri(REDIRECT_SCHEME + ":/" + REDIRECT_PATH),
                isUsingNativeUI: true
            );

            Authenticator.Completed += (sender, e) => {
                Console.WriteLine(e.IsAuthenticated);
            };

            Authenticator.AccessTokenUrl = new Uri(ACCESSTOKEN_URI);
        }
    }
}
