﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Android;
using Android.App;
using Android.Content;
using Android.Net;
using Android.OS;
using Android.Util;
using TheKitchen.ViewModels;

namespace TheKitchen.Services
{
    [Service]
    public class PeriodicService : Service
    {
        static readonly string TAG = "X:" + typeof(PeriodicService).Name;
        static readonly int TimerWait = 900000; // half hour
        //static readonly int TimerWait = 360000; // .1 hour
        Timer timer;
        DateTime startTime;
        bool isStarted = false;

        public override void OnCreate()
        {
            base.OnCreate();
        }

        public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startId)
        {
            Log.Debug(TAG, $"OnStartCommand called at {startTime}, flags={flags}, startid={startId}");
            if (isStarted)
            {
                TimeSpan runtime = DateTime.UtcNow.Subtract(startTime);
                Log.Debug(TAG, $"This service was already started, it's been running for {runtime:c}.");
            }
            else
            {
                startTime = DateTime.UtcNow;
                Log.Debug(TAG, $"Starting the service, at {startTime}.");
                timer = new Timer(HandleTimerCallback, startTime, 0, TimerWait);
                isStarted = true;
            }
            return StartCommandResult.NotSticky;
        }

        public override IBinder OnBind(Intent intent)
        {
            // This is a started service, not a bound service, so we just return null.
            return null;
        }


        public override void OnDestroy()
        {
            timer.Dispose();
            timer = null;
            isStarted = false;

            TimeSpan runtime = DateTime.UtcNow.Subtract(startTime);
            Log.Debug(TAG, $"Simple Service destroyed at {DateTime.UtcNow} after running for {runtime:c}.");
            base.OnDestroy();
        }

        void HandleTimerCallback(object state)
        {
            TimeSpan runTime = DateTime.UtcNow.Subtract(startTime);
            Log.Debug(TAG, $"This service has been running for {runTime:c} (since ${state}).");
            //App.Current.MainPage.DisplayAlert("Service", "blah blah", "OK");



            Sync();
            //SendNotification();

        }

        void Sync()
        {
            // do syncing

            Task.Run(async () =>
            {
                //NetworkStatus internetStatus = Reachability.InternetConnectionStatus();
                ConnectivityManager connectivityManager = (ConnectivityManager)GetSystemService(ConnectivityService);

                if (connectivityManager.ActiveNetworkInfo.Type == ConnectivityType.Wifi)
                {
                    var setvm = new SettingsViewModel();
                    var result = await setvm.Sync();

                    if (result == true)
                    {
                        SendNotification("synced");
                    } else
                    {
                        SendNotification("Synced Failed, try manual sync or re-login");
                    }

                    
                } else
                {
                    SendNotification("Not Syncing - Requires Wifi");
                }
            });

        }

        void SendNotification(string msg)
        {
            Notification.Builder notificationBuilder = new Notification.Builder(this)
           .SetSmallIcon(Resource.Drawable.EditBoxDropDownLightFrame)
           .SetContentTitle("Kitchen Service")
           .SetContentText(msg);

            var notificationManager = (NotificationManager)GetSystemService(NotificationService);
            notificationManager.Notify(1, notificationBuilder.Build());
        }


    }
}

