﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TheKitchen.Models;

namespace TheKitchen.Services.Misc
{
    public static class Attributes
    {
        public static T GetAttribute<TAttribute, T>(this Enum value, Func<TAttribute, T> field)
        where TAttribute : Attribute
        {
            //var type = value.GetType();
            //var name = Enum.GetName(type, value);
            //var val = type.GetField(name) // I prefer to get attributes this way
            //    .GetCustomAttributes(false)
            //    .OfType<TAttribute>()
            //    .SingleOrDefault();

            var val = GetAttribute<TAttribute>(value);
            return field.Invoke(val);

        }

        public static TAttribute GetAttribute<TAttribute>(this Enum value)
        {
            var type = value.GetType();
            var name = Enum.GetName(type, value);
            var val = type.GetField(name) // I prefer to get attributes this way
                .GetCustomAttributes(false)
                .OfType<TAttribute>()
                .SingleOrDefault();

            return val;
        }


    }
    

    public class DisplayAttribute : Attribute
    {
        public DisplayAttribute(string @short, string @long, UnitMeasureType type)
        {
            Short = @short;
            Long = @long;
            MeasureType = type;
        }

        public string Short { get; set; }
        public string Long { get; set; }
        public UnitMeasureType MeasureType { get; set; }

    }
}
