﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TheKitchen.Data.Models;
using TheKitchen.Models;
using static TheKitchen.KitchenDatabase;

namespace TheKitchen.Services.DataStores
{
    public interface IDataStore<T>
    {
        Task<bool> AddItemAsync(T item);
        Task<bool> UpdateItemAsync(T item, bool changeLastModDate = true);
        Task<bool> DeleteItemAsync(T item);
        Task<bool> ConsumeItemAsync(T item);

        Task<T> GetItemAsync(Guid id);

        Task<IEnumerable<T>> GetItemsAsync(bool onHand, bool includeUsedAndDeleted = false, bool forceRefresh = false);

        Task<MiscInfo> GetMiscInfo(MiscInfoKey key);
        Task<int> StoreMiscInfo(MiscInfoKey key, string value);

        Task<IEnumerable<ActionLogItem>> GetLogEntries(bool synced);
        Task SetLogEntriesSynced(IEnumerable<ActionLogItem> items);
    }
}
