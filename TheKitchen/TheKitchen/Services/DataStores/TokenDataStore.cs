﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TheKitchen.Data.Models;
using TheKitchen.Models;
using Xamarin.Forms;



[assembly: Xamarin.Forms.Dependency(typeof(TheKitchen.Services.DataStores.TokenDataStore))]
namespace TheKitchen.Services.DataStores
{
    public class TokenDataStore : IDataStore<LoginToken>
    {
        public TokenDataStore()
        {
        }

        public async Task<bool> AddItemAsync(LoginToken item)
        {
            await GetDb().StoreTokenAsync(item);
            return true;
        }

        public Task<bool> ConsumeItemAsync(LoginToken item)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteItemAsync(LoginToken item)
        {
            throw new NotImplementedException();
        }

        public async Task<LoginToken> GetItemAsync(Guid id)
        {
            return await GetDb().GetTokenAsync();
        }

        public Task<IEnumerable<LoginToken>> GetItemsAsync(bool onHand, bool includeDeleted = false, bool forceRefresh = false)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<ActionLogItem>> GetLogEntries(bool synced)
        {
            throw new NotImplementedException();
        }

        public Task<MiscInfo> GetMiscInfo(KitchenDatabase.MiscInfoKey key)
        {
            throw new NotImplementedException();
        }

        public Task SetLogEntriesSynced(IEnumerable<ActionLogItem> items)
        {
            throw new NotImplementedException();
        }

        public Task<int> StoreMiscInfo(KitchenDatabase.MiscInfoKey key, string value)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateItemAsync(LoginToken item, bool changeLastModDate = true)
        {
            throw new NotImplementedException();
        }

        private KitchenDatabase GetDb()
        {
            string path = StaticConfigs.GetDatabasePath();
            return new KitchenDatabase(path);
        }
    }
}