﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TheKitchen.Data.Models;
using TheKitchen.Models;
using Xamarin.Forms;
using static TheKitchen.KitchenDatabase;

[assembly: Xamarin.Forms.Dependency(typeof(TheKitchen.Services.DataStores.ConsumableDataStore))]
namespace TheKitchen.Services.DataStores
{
    public class ConsumableDataStore : IDataStore<ConsumableItem>
    {
        //List<ConsumableItem> items;

        public ConsumableDataStore()
        {
            //items = new List<ConsumableItem>();
            //var mockItems = new List<ConsumableItem>
            //{
            //    new ConsumableItem { Id = Guid.NewGuid(), Name = "Eggs", Quantity = 1, UnitOfMeasure = UnitMeasure.Count, Price=.13 },
            //    new ConsumableItem { Id = Guid.NewGuid(), Name = "Bacon", Quantity = 500, UnitOfMeasure= UnitMeasure.Oz, Price=.5 },
            //    //new Item { Id = Guid.NewGuid().ToString(), Text = "Third item", Description="This is an item description." },
            //    //new Item { Id = Guid.NewGuid().ToString(), Text = "Fourth item", Description="This is an item description." },
            //    //new Item { Id = Guid.NewGuid().ToString(), Text = "Fifth item", Description="This is an item description." },
            //    //new Item { Id = Guid.NewGuid().ToString(), Text = "Sixth item", Description="This is an item description." },
            //};

            //foreach (var item in mockItems)
            //{
            //    items.Add(item);
            //}
        }

        public async Task<bool> AddItemAsync(ConsumableItem item)
        {
            await GetDb().InsertItems(new List<ConsumableDataItem>
                {
                    item.GetDataItem()
                }
            );

            ///items.Add(item);

            return await Task.FromResult(true);
        }

        /// <summary>
        /// When updating item from sync (from server), changeLastModDate is false
        /// This means the application did not mod it.
        /// </summary>
        /// <param name="item"></param>
        /// <param name="changeLastModDate"></param>
        /// <returns></returns>
        public async Task<bool> UpdateItemAsync(ConsumableItem item, bool changeLastModDate = true)
        {
            try
            {
                var db = GetDb();
                var dbItem = item.GetDataItem();
                await db.SaveItemAsync(dbItem, changeLastModDate);
                
                return await Task.FromResult(true);
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public async Task<bool> DeleteItemAsync(ConsumableItem item)
        {
            await GetDb().DeleteItemAsync(item.GetDataItem());
            return true;
        }

        public async Task<ConsumableItem> GetItemAsync(Guid id)
        {
            //return await Task.FromResult(items.FirstOrDefault(s => s.Id == id));
            var dbItem = await GetDb().GetItemAsync(id);
            return new ConsumableItem(dbItem);
        }

        public async Task<IEnumerable<ActionLogItem>> GetLogEntries(bool synced)
        {
            return await GetDb().GetLogEntries(synced);
        }

        public async Task SetLogEntriesSynced(IEnumerable<ActionLogItem> items)
        {
            await GetDb().SetLogEntriesSync(items);
        }

        public async Task<IEnumerable<ConsumableItem>> GetItemsAsync(bool onHand, bool includeUsedAndDeleted = false, bool forceRefresh = false)
        {
            var retList = new List<ConsumableItem>();
            var items = await GetDb().GetItemsAsync(onHand, includeUsedAndDeleted);

            foreach (var item in items)
            {
                retList.Add(new ConsumableItem(item));
            }

            return retList;
        }


        private KitchenDatabase GetDb()
        {
            string path = StaticConfigs.GetDatabasePath();
            return new KitchenDatabase(path);
        }

        public async Task<bool> ConsumeItemAsync(ConsumableItem item)
        {
            var db = GetDb();

            await db.ConsumeItemAsync(item.GetDataItem());

            return true;
        }


        public async Task<MiscInfo> GetMiscInfo(MiscInfoKey key)
        {
            var db = GetDb();
            return await db.GetMiscInfo(key);
        }

        public async Task<int> StoreMiscInfo(MiscInfoKey key, string value)
        {
            var db = GetDb();
            return await db.StoreMiscInfo(key, value);
        }

    }
}