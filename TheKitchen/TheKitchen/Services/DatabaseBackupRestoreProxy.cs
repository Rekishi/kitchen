﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using TheKitchen.Models;

namespace TheKitchen.Services
{
    internal class DatabaseBackupRestoreProxy
    {
        public async Task Backup()
        {
            BackupLocal();
        }

        public async Task Restore(BackupItem item)
        {
            RestoreLocal(item);
        }

        private void BackupLocal()
        {
            var dbPath = StaticConfigs.GetDatabasePath();

            var dbBackUpPath = Path.Combine(
                StaticConfigs.GetLocalFilePath(),
                BackupItem.GetFileName(DateTime.Now)
                );

            File.Copy(dbPath, dbBackUpPath);
        }


        private void RestoreLocal(BackupItem item)
        {
            var dbPath = StaticConfigs.GetDatabasePath();

            var dbBackUpPath = Path.Combine(
                StaticConfigs.GetLocalFilePath(),
                item.FileName
                );


            if (File.Exists(dbPath))
            {
                File.Delete(dbPath);
            }

            File.Copy(dbBackUpPath, dbPath);
        }

        public async Task<List<BackupItem>> GetLocalBackups()
        {
            var ret = new List<BackupItem>();

            var files = System.IO.Directory.GetFiles(StaticConfigs.GetLocalFilePath());
            foreach (var item in files)
            {
                try {

                    var jsutFilename = Path.GetFileName(item);
                    ret.Add(new BackupItem(jsutFilename));

                } catch
                {
                    // continue on./...
                }
            }

            return ret;
        }

    }
}
