﻿using System;
using System.Collections.Generic;
using System.Linq;
using TheKitchen.Services.Misc;

namespace TheKitchen.Services
{
    internal class Enums<T> where T : struct, IConvertible
    {
        public IEnumerable<T> GetEnumList()
        {
            return Enum.GetValues(typeof(T)).Cast<T>();
        }

        public IEnumerable<string> GetDescriptionShortList()
        {
            var retList = new List<string>();
            var enumList = GetEnumList();
            foreach (var item in enumList) {
                retList.Add(Attributes.GetAttribute<DisplayAttribute, string>((item as Enum), x => x.Short));
            }

            return retList;
        }
    }
}
