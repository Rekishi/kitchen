﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace TheKitchen.Services
{
    internal static class StaticConfigs
    {
        internal static string GetLocalFilePath()
        {
            return System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
        }

        internal static string GetDatabasePath()
        {
            return Path.Combine(GetLocalFilePath(), "DatabaseName.db3");
        }

        internal static string Token { get; set; }
    }
}
