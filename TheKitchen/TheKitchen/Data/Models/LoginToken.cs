﻿using System;
using SQLite;


namespace TheKitchen.Models
{
    public class LoginToken
    {
        [PrimaryKey]
        public Guid Id { get; set; }

        public string access_token { get; set; }
        public string expires_in { get; set; }
        public string id_token { get; set; }
        public string refresh_token { get; set; }
        public string token_type { get; set; }

        public DateTime ExpireDateTime { get; set; }
    }

   
}
