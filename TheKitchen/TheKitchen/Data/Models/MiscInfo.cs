﻿using System;
using SQLite;


namespace TheKitchen.Models
{
    public class MiscInfo
    {
        [PrimaryKey]
        public Guid Id { get; set; }

        public string Key { get; set; }
        public string Value { get; set; }
    }
}
