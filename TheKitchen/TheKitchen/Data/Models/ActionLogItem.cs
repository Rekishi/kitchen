﻿using System;
using SQLite;

namespace TheKitchen.Data.Models
{
    public class ActionLogItem
    {
        [PrimaryKey]
        public Guid Id { get; set; }

        public Guid ConsumableId { get; set; }
        public int ActionId { get; set; }
        public DateTime When { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        //public int Qty { get; set; }
        public bool Synced { get; set; }
    }
}
