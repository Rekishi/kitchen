﻿using System;
using SQLite;


namespace TheKitchen.Models
{
    public class ConsumableDataItem
    {
        [PrimaryKey]
        public Guid Id { get; set; }

        public Guid KitchenId { get; set; }

        public string Name { get; set; }
        public double Price { get; set; }
        public int UnitOfMeasure { get; set; }
        public decimal Quantity { get; set; }
        public DateTime? Expiration { get; set; }
        public bool OnHand { get; set; }
        public bool GotItShopping { get; set; }

        public string FromUser { get; set; }

        public DateTime? ListAddedDate { get; set; }
        public DateTime? GotItShoppingDate { get; set;}
        public DateTime? AllUsedDate { get; set; }
        public bool ManuallyDeleted { get; set; }
        public bool SyncedAllUsedOrDeleted { get; set; }
        public DateTime? LastModDate { get; set; }
    }
}
