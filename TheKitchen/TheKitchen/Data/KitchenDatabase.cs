﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheKitchen.Data;
using TheKitchen.Data.Models;
using TheKitchen.Models;

namespace TheKitchen
{
    public class KitchenDatabase
    {
        readonly SQLiteAsyncConnection database;

        private List<ConsumableDataItem> GetDummyItems()
        {
            return new List<ConsumableDataItem>()
            {
                 new ConsumableDataItem()
                 {
                      Id = Guid.NewGuid(),
                       Expiration = DateTime.Now.AddDays(15),
                        Name = "Eggs db",
                         Price = 1,
                          Quantity = 20,
                           UnitOfMeasure = 1
                 },
                 new ConsumableDataItem()
                 {
                      Id = Guid.NewGuid(),
                       Expiration = DateTime.Now.AddDays(15),
                        Name = "Bacon db",
                         Price = 2,
                          Quantity = 3,
                           UnitOfMeasure = 2
                 },
            };
        }

        public KitchenDatabase(string dbPath)
        {
            database = new SQLiteAsyncConnection(dbPath);
            database.CreateTableAsync<ConsumableDataItem>().Wait();
            database.CreateTableAsync<LoginToken>().Wait();
            database.CreateTableAsync<ActionLogItem>().Wait();
            database.CreateTableAsync<MiscInfo>().Wait();

            //database.DropTableAsync<MiscInfo>();


            // temp insert items
            //database.QueryAsync<ConsumableDataItem>("delete FROM ConsumableDataItem");
            //InsertItems(GetDummyItems());
        }

        public Task<List<ConsumableDataItem>> GetItemsAsync(bool onHand, bool includeUsedAndDeleted)
        {
            if (includeUsedAndDeleted)
            {
                return database.Table<ConsumableDataItem>().Where(x => x.OnHand == onHand).ToListAsync();
            }
            return database.Table<ConsumableDataItem>().Where(x =>
                    x.OnHand == onHand &&
                    x.ManuallyDeleted == false &&
                    x.Quantity > 0
                ).ToListAsync();
        }

        public async Task<bool> ConsumeItemAsync(ConsumableDataItem itemIn)
        {
            var item = await database.Table<ConsumableDataItem>().Where(x => x.Id == itemIn.Id).FirstOrDefaultAsync();

            if (item != null)
            {
                var usedQty = item.Quantity - itemIn.Quantity;

                item.Quantity = usedQty;

                if (item.Quantity <= 0)
                {
                    item.AllUsedDate = DateTime.Now.ToUniversalTime();

                }

                item.LastModDate = DateTime.Now.ToUniversalTime();
                await database.UpdateAsync(item);

                await Log(1, itemIn.Id, string.Empty, usedQty.ToString());

                return true;
            }

            return true;

        }

        private async Task Log(int actionId, Guid itemId, string from, string to)
        {
            var logEntry = new ActionLogItem()
            {
                Id = Guid.NewGuid(),
                ActionId = actionId,
                ConsumableId = itemId,
                From = from,
                To = to,
                When = DateTime.Now.ToUniversalTime()
            };

            await database.InsertAsync(logEntry);
        }


        public async Task<IEnumerable<ActionLogItem>> GetLogEntries(bool synced)
        {
            return await database.Table<ActionLogItem>().Where(x => x.Synced == synced).ToListAsync();
        }

        public async Task SetLogEntriesSync(IEnumerable<ActionLogItem> items)
        {
            foreach (var item in items)
            {
                item.Synced = true;

            }
            await database.UpdateAllAsync(items);
        }


        public Task<ConsumableDataItem> GetItemAsync(Guid id)
        {
            return database.Table<ConsumableDataItem>().Where(i => i.Id == id).FirstOrDefaultAsync();
        }

        /// <summary>
        /// When updating item from sync (from server), changeLastModDate is false
        /// This means the application did not mod it.
        /// </summary>
        /// <param name="item"></param>
        /// <param name="changeLastModDate"></param>
        /// <returns></returns>
        public async Task<int> SaveItemAsync(ConsumableDataItem item, bool changeLastModDate = true)
        {
            var itemCheck = await GetItemAsync(item.Id);

            if (changeLastModDate)
            {
                item.LastModDate = DateTime.Now.ToUniversalTime();
                var changeCheck = (new ChangeDiscoveryForUpdate(itemCheck, item)).GetChangeLogs();
                foreach (var changeItem in changeCheck)
                {
                    await Log(changeItem.Type, itemCheck.Id, changeItem.From, changeItem.To);
                }
            }


            if (item.Id != Guid.Empty)
            {

                if (itemCheck == null)
                {
                    return await database.InsertAsync(item);
                }
                else
                {
                    return await database.UpdateAsync(item);
                }
            }
            else
            {
                item.Id = Guid.NewGuid();
                return await database.InsertAsync(item);
            }
        }


        public Task<int> StoreTokenAsync(LoginToken token)
        {
            database.QueryAsync<ConsumableDataItem>("delete FROM LoginToken");
            token.Id = Guid.NewGuid();
            token.ExpireDateTime = DateTime.Now.AddDays(30); //DateTime.Now.AddSeconds(int.Parse(token.expires_in));
            return database.InsertAsync(token);
        }

        public Task<LoginToken> GetTokenAsync()
        {
            return database.Table<LoginToken>().FirstOrDefaultAsync();
        }


        public async Task<int> InsertItems(IEnumerable<ConsumableDataItem> newItems)
        {
            foreach (var item in newItems)
            {
                item.LastModDate = DateTime.Now.ToUniversalTime();

                await Log(10, item.Id, string.Empty, item.Quantity.ToString());
            }



            return await database.InsertAllAsync(newItems);
        }


        public async Task<int> DeleteItemAsync(ConsumableDataItem item)
        {
            await Log(9, item.Id, string.Empty, string.Empty);
            return await database.DeleteAsync(item);
        }


        public async Task<MiscInfo> GetMiscInfo(MiscInfoKey key)
        {
            try
            {

                var stringkey = key.ToString();
                return await database.Table<MiscInfo>().Where(x => x.Key == stringkey).FirstOrDefaultAsync();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return null;
            }
        }

        public async Task<int> StoreMiscInfo(MiscInfoKey key, string value)
        {
            try
            {
                var item = await GetMiscInfo(key);
                string stringkey = key.ToString();
                if (item != null)
                {
                    await database.DeleteAsync(item);
                }

                var newItem = new MiscInfo()
                {
                    Id = Guid.NewGuid(),
                    Key = stringkey,
                    Value = value
                };


                await database.InsertAsync(newItem);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return -1;
            }

            return 1;
        }


        public enum MiscInfoKey
        {
            LastSync = 0
        }

    }
}
