﻿using System;
using System.Collections.Generic;
using TheKitchen.Models;

namespace TheKitchen.Data
{
    internal class ChangeDiscoveryForUpdate
    {
        ConsumableDataItem itemFromData;
        ConsumableDataItem itemFromUpdate;

        public ChangeDiscoveryForUpdate(ConsumableDataItem itemFromData, ConsumableDataItem itemFromUpdate)
        {
            this.itemFromData = itemFromData;
            this.itemFromUpdate = itemFromUpdate;
        }

        public List<ChangeLogItem> GetChangeLogs()
        {
            var ret = new List<ChangeLogItem>();

            if (itemFromData.Quantity != itemFromUpdate.Quantity)
            {
                ret.Add(new ChangeLogItem()
                {
                    Type = 2,
                    From = itemFromData.Quantity.ToString(),
                    To = itemFromUpdate.Quantity.ToString()
                });
            }

            if (itemFromData.Name != itemFromUpdate.Name)
            {
                ret.Add(new ChangeLogItem()
                {
                    Type = 3,
                    From = itemFromData.Name,
                    To = itemFromUpdate.Name
                });
            }

            string expirationFromSafe = GetSafeLogDate(itemFromData.Expiration);
            string expirationToSafe = GetSafeLogDate(itemFromUpdate.Expiration);

            if (expirationFromSafe != expirationToSafe)
            {
                ret.Add(new ChangeLogItem()
                {
                    Type = 4,
                    From = expirationFromSafe,
                    To = expirationToSafe
                });
            }

            if (itemFromData.UnitOfMeasure != itemFromUpdate.UnitOfMeasure)
            {
                ret.Add(new ChangeLogItem()
                {
                    Type = 5,
                    From = itemFromData.UnitOfMeasure.ToString(),
                    To = itemFromUpdate.UnitOfMeasure.ToString()
                });
            }

            if (itemFromData.Price != itemFromUpdate.Price)
            {
                ret.Add(new ChangeLogItem()
                {
                    Type = 6,
                    From = itemFromData.Price.ToString(),
                    To = itemFromUpdate.Price.ToString()
                });
            }

            return ret;
        }

        public string GetSafeLogDate(DateTime? dateIn)
        {
            if (dateIn.HasValue)
            {
                return dateIn.ToString();
            } else
            {
                return "";
            }
        }


        internal class ChangeLogItem
        {
            public int Type { get; set; }
            public string From { get; set; }
            public string To { get; set; }
        }
    }
}
