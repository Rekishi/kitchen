﻿using System;
using System.Collections.Generic;
using TheKitchen.Services;
using TheKitchen.Services.Misc;
using TheKitchen.ViewModels;

namespace TheKitchen.Models
{
    public class ConsumableItem : BaseViewModel, Validateable
    {
        public ConsumableItem() { }

        public ConsumableItem(Models.ConsumableDataItem dataItem)
        {
            this.Id = dataItem.Id;
            this.Name = dataItem.Name;
            this.Price = dataItem.Price;
            this.Quantity = dataItem.Quantity;
            this.UnitOfMeasure = (UnitMeasure)dataItem.UnitOfMeasure;
            this.Expiration = dataItem.Expiration;
            this.OnHand = dataItem.OnHand;
            this.GotItShopping = dataItem.GotItShopping;

            this.FromUser = dataItem.FromUser;
            this.ListAddedDate = dataItem.ListAddedDate;
            this.GotItShoppingDate = dataItem.GotItShoppingDate;
            this.AllUsedDate = dataItem.AllUsedDate;
            this.ManuallyDeleted = dataItem.ManuallyDeleted;
            this.SyncedAllUsedOrDeleted = dataItem.SyncedAllUsedOrDeleted;

            this.LastModDate = dataItem.LastModDate;
        }

        public static ConsumableItem GetNewItem(bool onHand)
        {
            return new ConsumableItem
            {
                Id = Guid.NewGuid(),
                Name = "",
                UnitOfMeasure = UnitMeasure.Count,
                Price = 1,
                Expiration = DateTime.Now.AddDays(15),
                Quantity = 1,
                OnHand = onHand,
                GotItShopping = false,
                FromUser = "You",
                ListAddedDate = DateTime.Now,
                GotItShoppingDate = null,
                AllUsedDate = null,
                ManuallyDeleted = false,
                SyncedAllUsedOrDeleted = false,
                LastModDate = DateTime.Now
            };
        }

        public ConsumableDataItem GetDataItem()
        {
            return new ConsumableDataItem
            {
                Id = this.Id == Guid.Empty ? Guid.NewGuid() : this.Id,
                Expiration = this.Expiration,
                Name = this.Name,
                Price = this.Price,
                Quantity = this.Quantity,
                UnitOfMeasure = (int)this.UnitOfMeasure,
                OnHand = this.OnHand,
                GotItShopping = this.GotItShopping,
                FromUser = this.FromUser,
                ListAddedDate = this.ListAddedDate,
                AllUsedDate = this.AllUsedDate,
                GotItShoppingDate = this.GotItShoppingDate,
                ManuallyDeleted = this.ManuallyDeleted,
                SyncedAllUsedOrDeleted = this.SyncedAllUsedOrDeleted,
                LastModDate = this.LastModDate
            };
        }

        private bool gotIt;
        public bool GotItShopping
        {
            get
            {
                return gotIt;
            }
            set
            {
                SetProperty(ref gotIt, value);
            }
        }

        public string FromUser { get; set; }
        public DateTime? ListAddedDate { get; set; }
        public DateTime? GotItShoppingDate { get; set; }
        public DateTime? AllUsedDate { get; set; }
        public bool ManuallyDeleted { get; set; }
        public bool SyncedAllUsedOrDeleted { get; set; }
        public Guid Id { get; set; }

        public DateTime? LastModDate { get; set; }

        public string Name { get; set; }

        public bool OnHand { get; set; }

        double price;
        public double Price
        {
            get
            {
                return price;
            }
            set
            {
                SetProperty(ref price, value);
            }
        }


        public UnitMeasure unitOfMeasure;
        public UnitMeasure UnitOfMeasure
        {
            get { return unitOfMeasure; }
            set
            {
                //SetProperty(ref unitOfMeasure, value);
                unitOfMeasure = value;
            }
        }

        public UnitMeasure UnitOfMeasurePickerBinding
        {
            get { return UnitOfMeasure; }
            set
            {
                //SetProperty(ref unitOfMeasure, value);
                if (value != UnitMeasure.Unk)
                {
                    UnitOfMeasure = value;
                }
            }
        }


        public string UnitOfMeasureStringShort
        {
            get
            {
                return UnitMeasureDisplay(x => x.Short);
            }
        }
        public string UnitOfMeasureStringLong
        {
            get
            {
                return UnitMeasureDisplay(x => x.Long);
            }
        }

        private string UnitMeasureDisplay(Func<DisplayAttribute, string> func)
        {
            return func.Invoke(UnitOfMeasure.GetAttribute<DisplayAttribute>());
        }


        public decimal Quantity { get; set; }
        public DateTime? Expiration { get; set; }
        public string UnitOfMeasureString
        {
            get
            {
                return UnitOfMeasure.ToString();
            }
        }

        public IEnumerable<UnitMeasure> UnitOfMeasureList
        {
            get
            {
                return (new Enums<UnitMeasure>()).GetEnumList();
            }

        }


        public ModelErrors Errors { get; set; }

        public ModelErrors Validate()
        {
            Errors = new ModelErrors();

            if (string.IsNullOrEmpty(Name))
            {
                Errors.AddError("Name must be filled in");
            }

            return Errors;
        }





    }
}
