﻿using System;
using System.Collections.Generic;

namespace TheKitchen.Models
{
    public class ModelErrors
    {
        public ModelErrors()
        {
            Errors = new List<String>();
        }

        private List<string> Errors { get; set; }
        
        public void AddError(string error)
        {
            Errors.Add(error);
        }

        public IEnumerable<string> GetErrors()
        {
            return Errors;
        }

        public bool HasErrors()
        {
            return Errors.Count > 0;
        }

    }
}
