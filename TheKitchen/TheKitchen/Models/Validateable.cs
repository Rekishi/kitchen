﻿namespace TheKitchen.Models
{
    interface Validateable
    {
        ModelErrors Validate();
    }
}
