﻿using System;
using System.Collections.Generic;
using System.Text;
using TheKitchen.Services.Misc;

namespace TheKitchen.Models
{
    public enum UnitMeasureType
    {
        Unk = 0,
        Volume = 1,
        Weight = 2,
        Count = 3
    }

    public enum UnitMeasure
    {
        [Display("UNK","Unknown", UnitMeasureType.Unk)]
        Unk = 0,

        [Display("(Cnt)","Count", UnitMeasureType.Count)]
        Count = 1,

        [Display("(Lb)", "Pound (Lb)", UnitMeasureType.Weight)]
        Lb = 2,

        [Display("(Fl Oz)", "Fluid Ounce (Oz)", UnitMeasureType.Volume)]
        FlOz = 3,

        [Display("(Gal)", "Gallon (Gal)", UnitMeasureType.Volume )]
        Gal = 4,

        [Display("(W Oz)", "Ounce - Weight (Oz)", UnitMeasureType.Weight)]
        WOz = 5,

        [Display("(Cup)", "Cups", UnitMeasureType.Volume)]
        Cup = 6,
    }
        
    public static class UnitConversion
    {
        public static decimal Convert(decimal value, UnitMeasure from, UnitMeasure to)
        {
            if (from.GetAttribute<DisplayAttribute>().MeasureType != to.GetAttribute<DisplayAttribute>().MeasureType)
            {
                throw new Exception("Invalid conversion types!");
            }


            decimal ret = 0;


            if (from == UnitMeasure.Cup && to == UnitMeasure.FlOz)
            {
                ret = value * 8m;
            } else if (from == UnitMeasure.FlOz && to == UnitMeasure.Cup)
            {
                ret = value / 8m;
            }
            else if (from == UnitMeasure.Cup && to == UnitMeasure.Gal)
            {
                ret = value * .0625m;
            }
            else if (from == UnitMeasure.Gal && to == UnitMeasure.Cup)
            {
                ret = value * 16m;
            }
            else if (from == UnitMeasure.FlOz && to == UnitMeasure.Gal)
            {
                ret = value * .0078125m;
            }
            else if (from == UnitMeasure.Gal && to == UnitMeasure.FlOz)
            {
                ret = value * 128m;
            }



            else if (from == UnitMeasure.WOz && to == UnitMeasure.Lb)
            {
                ret = value * .0625m;
            }

            else if (from == UnitMeasure.Lb && to == UnitMeasure.WOz)
            {
                ret = value * 16m;
            }


            return ret;
        }
        
    }

    

}
