﻿using System;

namespace TheKitchen.Models
{
    public class BackupItem
    {
        public BackupItem(string fileName)
        {
            FileName = fileName;

            string timestamp = fileName
                .Replace("ba_", "")
                .Replace(".db3", "");

            var val = long.Parse(timestamp);

            var datetime = DateTimeOffset.FromUnixTimeSeconds(val);
            When = datetime.DateTime;

        }

        public DateTime When { get; private set; }
        public string FileName { get; private set; }

        public static string GetFileName(DateTime when)
        {
            var timestamp = new DateTimeOffset(when).ToUnixTimeSeconds();
            return $"ba_{timestamp}.db3";
        }

        //private BackupItem FromFilename(string fileName)
        //{
            
        //}
    }
}
