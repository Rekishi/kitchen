﻿using System;

namespace Data.Models
{
    public class ConsumableDataItem
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public int UnitOfMeasure { get; set; }
        public int Quantity { get; set; }
        public DateTime? Expiration { get; set; }
        public bool OnHand { get; set; }
    }
}
