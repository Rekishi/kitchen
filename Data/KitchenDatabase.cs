﻿using Data.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class KitchenDatabase
    {
        readonly SQLiteAsyncConnection database;

        private List<ConsumableDataItem> GetDummyItems()
        {
            return  new List<ConsumableDataItem>()
            {
                 new ConsumableDataItem()
                 {
                      Id = Guid.NewGuid(),
                       Expiration = DateTime.Now.AddDays(15),
                        Name = "Eggs db",
                         Price = 1,
                          Quantity = 20,
                           UnitOfMeasure = 1
                 },
                 new ConsumableDataItem()
                 {
                      Id = Guid.NewGuid(),
                       Expiration = DateTime.Now.AddDays(15),
                        Name = "Bacon db",
                         Price = 2,
                          Quantity = 3,
                           UnitOfMeasure = 2
                 },
            };
        }

        public KitchenDatabase(string dbPath)
        {
            database = new SQLiteAsyncConnection(dbPath);
            database.CreateTableAsync<ConsumableDataItem>().Wait();

            // temp insert items
            //InsertItems(GetDummyItems());
        }

        public Task<List<ConsumableDataItem>> GetItemsAsync()
        {
            return database.Table<ConsumableDataItem>().Where(x=>x.OnHand == true).ToListAsync();
        }

        public Task<List<ConsumableDataItem>> GetListItemsAsync()
        {
            return database.Table<ConsumableDataItem>().Where(x => x.OnHand == false).ToListAsync();
        }

        
            

        //public Task<List<ConsumableDataItem>> GetItemsNotDoneAsync()
        //{
        //    return database.QueryAsync<ConsumableDataItem>("SELECT * FROM [TodoItem] WHERE [Done] = 0");
        //}

        public Task<ConsumableDataItem> GetItemAsync(Guid id)
        {
            return database.Table<ConsumableDataItem>().Where(i => i.Id == id).FirstOrDefaultAsync();
        }

        public Task<int> SaveItemAsync(ConsumableDataItem item)
        {
            if (item.Id != Guid.Empty)
            {
                return database.UpdateAsync(item);
            }
            else
            {
                item.Id = Guid.NewGuid();
                return database.InsertAsync(item);
            }
        }


        public Task<int> InsertItems(IEnumerable<ConsumableDataItem> newItems)
        {
            return database.InsertAllAsync(newItems);
        }


        public Task<int> DeleteItemAsync(ConsumableDataItem item)
        {
            return database.DeleteAsync(item);
        }
    }
}
